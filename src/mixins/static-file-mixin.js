export default {
  methods: {
    getFile(path) {
      return `${process.env.VUE_APP_API}/storage/${path}`;
    },
  },
};
