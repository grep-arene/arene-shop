export const imageMixin = {
  methods: {
    imageURL (imagePath) {
      return `${process.env.VUE_APP_API}/storage/${imagePath}`
    }
  }
}
